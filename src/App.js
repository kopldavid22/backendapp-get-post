import React, { useState, useEffect, useCallback } from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import DataViewList from "./components/DataViewList";
import * as dayjs from "dayjs";
import * as isBetween from "dayjs/plugin/isBetween";
import { DateRange } from "react-date-range";
import "./App.css";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file

function App() {
  const [DataView, setDataView] = useState([]);
  const [range, setRange] = useState([
    {
      startDate: new Date("1-10-1940"),
      endDate: new Date("12-10-1940"),
      key: "selection",
    },
  ]);
  const [isLoading, setIsLoading] = useState(false);
  const [loadedDataLocation, setLoadedDataLocation] = useState([
    {
      poland: 0,
      france: 0,
      uk: 0,
      germany: 0,
      hungary: 0,
      austria: 0,
    },
  ]);
  const [error, setError] = useState(null);
  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  }

  const loadedDataView = [];
  const clearState = () => {
    let initState = [
      {
        poland: 0,
        france: 0,
        uk: 0,
        germany: 0,
        hungary: 0,
        austria: 0,
      },
    ];
    setLoadedDataLocation({ ...initState });
  };
  const fetchDataViewHandler = useCallback(async () => {
    setIsLoading(true);
    setError(null);
    try {
      const response = await fetch(
        "https://run.mocky.io/v3/a32f2030-32b0-4c5d-8ada-c3b67cf2cbfe"
      );
      if (!response.ok) {
        throw new Error("Something went wrong!");
      }
      const data = await response.json();

      if (loadedDataView.length === 0) {
        for (const key in data) {
          loadedDataView.push({
            id: key,
            firstName: data[key].first_name,
            lastName: data[key].last_name,
            email: data[key].email,
            birthday: data[key].birthday,
            country: data[key].country,
          });
        }
      }

      dayjs.extend(isBetween);
      // const restartCount = () => {
      //   let def = [
      //     {
      //       poland: 0,
      //       france: 0,
      //       uk: 0,
      //       germany: 0,
      //       hungary: 0,
      //       austria: 0,
      //     },
      //   ];
      //   setLoadedDataLocation(def);
      // };
      const counter = (date1, date2) => {
        console.log(date1, date2);
        for (const key in data) {
          if (
            dayjs(data[key].birthday.toString()).isBetween(date1, dayjs(date2))
          ) {
            if (data[key].country === "Poland") {
              loadedDataLocation[0].poland++;
            } else if (data[key].country === "France") {
              loadedDataLocation[0].france++;
            } else if (data[key].country === "Germany") {
              loadedDataLocation[0].germany++;
            } else if (data[key].country === "Hungary") {
              loadedDataLocation[0].hungary++;
            } else if (data[key].country === "Austria") {
              loadedDataLocation[0].austria++;
            } else if (data[key].country === "United Kingdom") {
              loadedDataLocation[0].uk++;
            }
            // loadedDataLocation.push({
            //   id: key,
            //   country: data[key].country,
            //   lastName: data[key].last_name,
            // });
          }
        }
      };

      counter(
        formatDate(range[0].startDate.toDateString()),
        formatDate(range[0].endDate.toDateString())
      ); //Just change data on click
      setLoadedDataLocation(loadedDataLocation);
      setDataView(loadedDataView);
    } catch (error) {
      setError(error.message);
    }
    setIsLoading(false);
  }, [
    loadedDataLocation,
    formatDate(range[0].startDate.toDateString()),
    formatDate(range[0].endDate.toDateString()),
  ]);

  useEffect(() => {
    fetchDataViewHandler();
  }, [fetchDataViewHandler, range]);

  let content = <p>Found no data.</p>;

  if (DataView.length > 0) {
    content = <DataViewList DataView={DataView} />;
  }

  if (error) {
    content = <p>{error}</p>;
  }

  if (isLoading) {
    content = <p>Loading...</p>;
  }
  const options = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: "pie",
    },
    title: {
      text: "Data location",
    },
    tooltip: {
      pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
    },
    accessibility: {
      point: {
        valueSuffix: "%",
      },
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: "pointer",
        dataLabels: {
          enabled: true,
          format: "<b>{point.name}</b>: {point.percentage:.1f} %",
        },
      },
    },
    series: [
      {
        name: "Brands",
        colorByPoint: true,
        data: [
          {
            name: "Poland",
            y: loadedDataLocation[0].poland,
            sliced: true,
            selected: true,
          },
          {
            name: "Hungary",
            y: loadedDataLocation[0].hungary,
          },
          {
            name: "Austria",
            y: loadedDataLocation[0].austria,
          },
          {
            name: "United Kingdom",
            y: loadedDataLocation[0].uk,
          },
          {
            name: "Germany",
            y: loadedDataLocation[0].germany,
          },
          {
            name: "France",
            y: loadedDataLocation[0].france,
          },
        ],
      },
    ],
  };

  return (
    <React.Fragment>
      <section>
        <DateRange
          editableDateInputs={true}
          onChange={(item) => setRange([item.selection])}
          onClick={clearState}
          moveRangeOnFirstSelection={false}
          ranges={range}
        />
        <HighchartsReact highcharts={Highcharts} options={options} />
      </section>
      <section>
        <button onClick={clearState}>Restart Graph</button>
      </section>
      <section>{content}</section>
    </React.Fragment>
  );
}

export default App;
