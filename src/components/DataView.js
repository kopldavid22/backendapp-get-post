import React from 'react';

import classes from './DataView.module.css';

const DataView = (props) => {
  return (
    <li className={classes.dataView}>
      <h2>
        {props.firstName} {props.lastName}
      </h2>
      <h3>{props.email}</h3>
      <p> {props.country}</p>
      <p>{props.birthday}</p>
    </li>
  );
};

export default DataView;
