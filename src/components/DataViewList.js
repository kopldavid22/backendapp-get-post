import React from 'react';

import DataView from './DataView';
import classes from './DataViewList.module.css';

const DataViewList = (props) => {
  return (
    <ul className={classes['data-view-list']}>
      {props.DataView.map((dataView) => (
        <DataView
          key={dataView.id}
          firstName={dataView.firstName}
          lastName={dataView.lastName}
          email={dataView.email}
          birthday={dataView.birthday}
          country={dataView.country}
        />
      ))}
    </ul>
  );
};

export default DataViewList;
