Spravte prosim jednoduchu reactovu aplikaciu, ktora nacita data z nasledujuceho endpointu:

https://run.mocky.io/v3/a32f2030-32b0-4c5d-8ada-c3b67cf2cbfe

su to pouzivatelia s datumom narodenia a krajinou povodu. Pouzite kniznicu highcharts a typ grafu pie chart https://www.highcharts.com/demo/pie-basic,
ktoreho casti budu znamenat krajinu, a hodnota bude pocet pouzivatelov v danej krajine.

Napravo od grafu zobrazte dva inputy, ktore budu sluzit ako datumovy filter na rozmedzie datumov od - do, 
ktory bude aplikovany na datum narodenia. Vyberte si vhodny plugin na datepicker a aplikujte ho na inputy.  
Po zvoleni datumov sa bude tento filter automaticky aplikovat na graf a zobrazi iba pouzivatelov, ktorych datum narodenia spada do zadaneho rozmedzia.  

Ked to budete mat hotove, dokladne si prosim aplikaciu otestujte a preverte, ci ste mysleli na vsetky sposoby pouzitia. 

Nebudeme to komplikovat s gitom, len to prosim zozipujte a poslite mailom.


SETUP
___
npm install
npm start
___
enjoy app 👨‍💻